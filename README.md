

La explicación del proyecto y del código relacionado 


Command line instructions

You can also upload existing files from your computer using the instructions below.


Git global setup

git config --global user.name "MisiónTIC UIS_JAS" <br>
git config --global user.email "misiontic.formador17@uis.edu.co"


Create a new repository

git clone https://gitlab.com/o53gxxtiendavirtual/o53gxxtiendavirtual.git <br>
cd o53gxxtiendavirtual <br>
git switch -c main <br>
touch README.md <br>
git add README.md <br>
git commit -m "add README" <br>
git push -u origin main


Push an existing folder

cd existing_folder <br>
git init --initial-branch=main <br>
git remote add origin https://gitlab.com/o53gxxtiendavirtual/o53gxxtiendavirtual.git <br>
git add . <br>
git commit -m "Initial commit" <br>
git push -u origin main


Push an existing Git repository

cd existing_repo <br>
git remote rename origin old-origin <br>
git remote add origin https://gitlab.com/o53gxxtiendavirtual/o53gxxtiendavirtual.git <br>
git push -u origin --all <br>
git push -u origin --tags

